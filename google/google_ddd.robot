*** Settings ***
Library    SeleniumLibrary
# Suite Setup     GoToGoogle
Test Setup      GoToGoogle
Test Template   Search Should Contain Text Itself

*** Keywords ***
GoToGoogle
    Open Browser    http://google.co.th     gc
Input Text To Search Box
    [Arguments]       ${keyword}
    Input Text        id:lst-ib        ${keyword}

Submit For Search Result
    Press Key        id:lst-ib         \\13

Search Should Contain Text Itself
    [Arguments]       ${search_keyword}
    Input Text To Search Box           ${search_keyword}
    Submit For Search Result
    Page Should Contain                ${search_keyword}

*** Test cases ***          Search Keywords
Search For Maqe             maqe
Search For Shopspot         7peak

