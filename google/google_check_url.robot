*** Settings ***
Library    SeleniumLibrary
Library    check_encode_string.py
# Suite Setup     GoToGoogle
Test Setup      GoToGoogle
Suite Teardown   Close Browser


*** Keywords ***
GoToGoogle
    Open Browser    http://google.co.th     gc
Input Text To Search Box
    [Arguments]       ${keyword}
    Input Text        id:lst-ib        ${keyword}

*** Test cases ***
Search For Maqe
    Input Text To Search Box           เฟสบุ้ค
    Press Key        id:lst-ib         \\13
    Page Should Contain                facebook
    ${url}=              Get Location
    ${found}=       url should contains      ${url}     เฟสบุ้ค
    Should Be True      ${found}

Search For 7Peak
    Input Text To Search Box            7Peak
    Press Key        id:lst-ib          \\13
    Page Should Contain                 7Peak