*** Settings ***
Library    SeleniumLibrary
# Suite Setup     GoToGoogle
Test Setup      GoToGoogle

*** Keywords ***
GoToGoogle
    Open Browser    http://google.co.th     gc
Input Text To Search Box
    [Arguments]       ${keyword}
    Input Text        id:lst-ib        ${keyword}

*** Test cases ***
Search For Maqe
    Input Text To Search Box           maqe
    Press Key        id:lst-ib         \\13
    Page Should Contain                maqe.com

Search For 7Peak
    Input Text To Search Box            7Peak
    Press Key        id:lst-ib          \\13
    Page Should Contain                 7Peak