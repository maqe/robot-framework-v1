import urllib.parse

def url_should_contains(url, keyword):
    keyword_encode = urllib.parse.quote(keyword)
    return keyword_encode in url