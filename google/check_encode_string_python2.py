# coding: utf-8
import urllib

def convert_to_urlencode(k):
    return urllib.quote(k.encode('utf8'))