var seatArray = ["A", "B", "L"];

$(".table-popup tr").each(function(index, zone) {
	const zoneTitle = $(zone).find("td:first-child")[0].textContent;
	const zoneSeats = $(zone).find("td:last-child")[0].textContent;

	if (seatArray.indexOf(zoneTitle) !== -1 && zoneSeats >= 2) {
		$('area[href*="'+zoneTitle+'"]').click();
		return false;
	}
});

