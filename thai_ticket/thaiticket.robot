*** Settings ***
Library                     SeleniumLibrary

*** Keywords ***
Login
    Wait Until Element Is Visible       id:username

    Input Text                          id:username             xxx.xxxx@gmail.com

    Input Text                          id:password             123456

    Execute Javascript                  $('#register2').prop('disabled', false);

    Click Element                       id:register2

KeepTryGoToConcert
    Go to                               ${ticketURL}
    Log To Console                      Trying to book${\n}
    Click Element                       class:buynow-button

GoToConcert

    Wait Until Keyword Succeeds         60m         5sec            KeepTryGoToConcert

    Wait Until Element Is Visible       id:rdId

    Run Keyword And Ignore Error        Select From List By Index           id:rdId                 ${timeslotIndex}

    Sleep                               2
    Execute JavaScript                  seatsAvailable('โซนที่นั่ง / Zone Availability');
    Wait Until Element Is Visible       class:table-popup
    Execute Javascript                  ${CURDIR}/selectArea.js

    TrySelectSeat

    # Checkout

CheckAlert
    ${hasNoAlert}=                        Run Keyword And Return Status       Alert Should Not Be Present
    Log To Console                        Pass?${hasNoAlert}${\n}
    Run Keyword Unless                     ${hasNoAlert}             TrySelectSeat

TrySelectSeat
    ${seatsCount}                       Execute JavaScript                  ${CURDIR}/select.js

    Log To Console                      Seat count = ${seatsCount}
    ${pass}                             Evaluate                            ${seatsCount} > 0

    Click Element                       id:booknow

    Run Keyword If                      ${pass}             CheckAlert
    Run Keyword Unless                  ${pass}             GoToConcert

Checkout
    Execute JavaScript                  $('#aigflag_disagree').click()
    Click Element                       id:protect_confirm

    Execute JavaScript                  $('#btn_mobile').click()
    Execute Javascript                  $('#mtype_kplus').click()

    Wait Until Element Is Visible       id:mobilecontact

    Input Text                          id:mobilecontact                0910714696

    Execute Javascript                  $('#rdagree_y').click()

    Execute Javascript                  $('#btn_pickup').click()

    Click Element                      idchannel_confirm

*** Variables ***

${ticketURL}                            http://www.thaiticketmajor.com/concert/concert-chantana-2018-th.html
${timeslotIndex}                        2
${seatArea}                             A1

*** Test cases ***

Buyticket
    Open Browser                        http://www.thaiticketmajor.com/register/        gc

    Login
    Wait Until Keyword Succeeds         60m         5sec            GoToConcert
