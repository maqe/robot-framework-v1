*** Settings ***
Library  Collections
Library  RequestsLibrary

*** Test Cases ***
Get Requests
    Create Session        github         https://api.github.com     verify=true
    ${resp}=    Get Request     github         /users/bulkan
    ${headers}=    Create Dictionary       Content-Type=application/x-www-form-urlencoded
    Should Be Equal As Strings        ${resp.status_code}         200