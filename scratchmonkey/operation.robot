
*** Keywords ***
User already login
    Open Browser    http://scratchmonkeys.qa.maqe.com/    gc
    Click Element    //*[@id="product_content"]/div[2]/div[1]/div/div[3]/a
    CLick Element    //*[@id="content"]/div/ul/li[2]/a
    Input Text        //*[@id="input-email-login"]        ${username}
    Input Text        //*[@id="input-password-login"]        ${password}
    Click Element    //*[@id="login"]/form/div[4]/input

User already add item to cart
    Click Element    //*[@id="top-links"]/ul/li[4]/a[2]

User checkout
    Go to      http://scratchmonkeys.qa.maqe.com/index.php?route=checkout/checkout
    Click Element    //*[@id="button-payment-address"]
    Wait Until Element Is Visible    //*[@id="collapse-payment-method"]/div/div[3]/div/input[1]
    Select Checkbox    //*[@id="collapse-payment-method"]/div/div[3]/div/input[1]
    Execute Javascript    window.scrollBy(0,450)
    Click Element    //*[@id="button-payment-method"]
    Wait Until Element is Visible        name:braintree-dropin-frame
    Select Frame    braintree-dropin-frame
    Wait Until Element Is Enabled    //*[@id="credit-card-number"]    15 seconds
    Input Text        //*[@id="credit-card-number"]        ${credit_card}
    Input Text        //*[@id="expiration"]                ${expire}
    Input Text        //*[@id="cvv"]                        ${cvv}
    Input Text        //*[@id="postal-code"]                ${postal}
    Unselect Frame
    Click Element    //*[@id="button_confirm"]
    Set Selenium Implicit Wait    15 seconds

User should be able to purchase
    Page Should Contain         ${Success}
