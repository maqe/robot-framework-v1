*** Settings ***
Library  Collections
Library  SeleniumLibrary

*** Test Cases ***
Headless Chrome - Open Browser
    ${chrome options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome options}   add_argument    headless
    Call Method    ${chrome options}   add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome options}
    Set Window Size       1500     1200

    Go to       http://google.com

    Capture Page Screenshot